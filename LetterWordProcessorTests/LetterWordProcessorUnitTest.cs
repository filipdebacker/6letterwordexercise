﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordProcessor;

namespace WordProcessorTests 
{
  [TestClass]
  public class LetterWordProcessorUnitTest {
    private int _numberOfCharacters;
    private LetterWordProcessor _letterWordProcessor;
    private const string CORRECT_TEST_FILE = @"myFile.txt";
    private const string EMPTY_TEST_FILE = @"emptyFile.txt";
    private const string TEST_FILE_NOT_EXISTING = @"notExistingFile.txt";
    private const string TEST_FILE_ONE_WORD = @"myFileWithOneWord.txt";

    [TestInitialize]
    public void Initialize() 
    {
      _numberOfCharacters = 6;
      _letterWordProcessor = new LetterWordProcessor();
    }

    [TestMethod]
    public void When_File_Contains_One_Word() 
    {
      var items = _letterWordProcessor.ProcessFile(CORRECT_TEST_FILE, _numberOfCharacters);
      
      Assert.AreEqual(1, items.Count);
      Assert.AreEqual("foobar", items.First());
    }

    [TestMethod]
    public void Result_Does_Not_Contain_Longer_Words() {
      var items = _letterWordProcessor.ProcessFile(CORRECT_TEST_FILE, _numberOfCharacters);

      Assert.AreEqual(1, items.Count);
      Assert.IsFalse(items.Contains("foobarx"));
    }

    [TestMethod]
    public void When_File_Is_Empty_Then_Return_Empty_List() {
      var items = _letterWordProcessor.ProcessFile(EMPTY_TEST_FILE, _numberOfCharacters);

      Assert.AreEqual(0, items.Count);
    }

    [TestMethod]
    public void When_File_Has_One_Word_Return_Empty_List() {
      var items = _letterWordProcessor.ProcessFile(TEST_FILE_ONE_WORD, _numberOfCharacters);

      Assert.AreEqual(0, items.Count);
    }

    [TestMethod]
    [ExpectedException(typeof(System.IO.FileNotFoundException))]
    public void When_File_Does_Not_Exist_Then_Throw() {
      _letterWordProcessor.ProcessFile(TEST_FILE_NOT_EXISTING, _numberOfCharacters);
    }
  }
}
