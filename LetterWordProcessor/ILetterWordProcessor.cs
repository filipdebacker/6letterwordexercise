﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WordProcessor
{
  public interface ILetterWordProcessor 
  {
    IList<string> ProcessFile(string fileName, int numberOfLetters);
  }
}
