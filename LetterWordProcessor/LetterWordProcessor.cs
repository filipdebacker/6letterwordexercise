﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Text;

namespace WordProcessor 
{
  public class LetterWordProcessor : ILetterWordProcessor
  {
    public IList<string> ProcessFile(string fileName, int numberOfLetters) 
    {
      var wordsFromFile = ReadFile(fileName);

      if (!wordsFromFile.Any()) 
      {
        return new List<string>();
      }

      var combinationOfWordsFromFile = GetAllCombinations(wordsFromFile);

      var result = Compare(wordsFromFile, combinationOfWordsFromFile, numberOfLetters);

      return result;
    }

    private IList<string> ReadFile(string fileName) 
    {
        return File.ReadAllLines(fileName, Encoding.UTF8);
    }

    private IList<string> GetAllCombinations(IList<string> wordsFromFile) 
    {
      var query =
        from word1 in wordsFromFile
        from word2 in wordsFromFile
        where word1 != word2
        select word1 + word2;

      return query.ToList();
    }

    private IList<string> Compare(IList<string> wordsFromFile, IList<string> combinationOfWordsFromFile, int numberOfLetters) 
    {
      return wordsFromFile
        .Intersect(combinationOfWordsFromFile)
        .Where(x => x.Length == numberOfLetters)
        .ToList();
    }
  }
}
