﻿using System;
using System.IO;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using WordProcessor;

namespace SixLetterWordExercise
{
  class Program 
  {
    static void Main(string[] args) 
    {
      var serviceProvider = new ServiceCollection()
        .AddSingleton<ILetterWordProcessor, LetterWordProcessor>()
        .BuildServiceProvider();

      var letterWordProcessor = serviceProvider.GetService<ILetterWordProcessor>();
      var result = letterWordProcessor.ProcessFile("input.txt", 6);

      Console.WriteLine("Result:");
      Console.WriteLine();

      foreach (var word in result) {
        Console.WriteLine(word);
      }

      Console.WriteLine();
      Console.WriteLine();
      Console.WriteLine("Finished!");
      Console.ReadLine();
    }
  }
}
